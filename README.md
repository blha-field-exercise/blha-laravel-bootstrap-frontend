# blha-laravel-bootstrap-frontend

By
[Siegfried Schweizer](https://gitlab.com/SiegfriedSchweizer) ([website](https://sigi-schweizer.de/))

This is the frontend part of a field exercise given to me by [BLHA](https://blha.brandenburg.de/). It may also serve as an
example for a simple web application where frontend and backend are completely decoupled, that is, the frontend communicates
with the backend via REST API only.

In order to emphasize the latter, the field exercise consists of two separate [Laravel](https://laravel.com/) projects:
one for the frontend and one for the backend.

This one, the frontend part, contains Blade templates that generate the user accessible pages of the project, and the client-side 
code which talks to the server-side REST API being found in the backend project. All client-side code is written in Vanilla 
Javascript, hence there are no extra dependencies.

Developed and tested under [Debian 11](https://www.debian.org/) with PHP v8.2.13, Composer v2.2.7, and Laravel v10.34.2.
You will likely need to install [Composer](https://getcomposer.org/download/) if you don't have it in order to run the project.

## Installation

You may clone this repository anywhere you want on your computer since it is intended to run on Laravel's built-in development
server. However you should already have the backend project, [blha-laravel-sanctum-backend](https://gitlab.com/blha-field-exercise/blha-laravel-sanctum-backend),
up and running.

```sh
git clone https://gitlab.com/blha-field-exercise/blha-laravel-bootstrap-frontend.git
cd blha-laravel-bootstrap-frontend
composer install
```

Now you will be able to run the thing:

```sh
php artisan serve --port=9000
```

Note that we are explicitly giving a port here, which must be different from the one the backend project is already listening on.

Now watch it in your Browser: http://127.0.0.1:9000/ for freshly registering or http://127.0.0.1:9000/login for logging in
with one of the credentials you may look up in the seeder file of the backend project.
