/**
 * Author: Siegfried Schweizer <sigi@sigi-schweizer.de> 2023
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * From now on only VanillaJS please :)
 *
 */

/*
 * Setting up global values and variables
 */
const registerForm = document.getElementById('register-form');
const registerButton = document.getElementById('register-button');
const backendUrl = 'http://127.0.0.1:8000/api/auth/register';

/*
 * Setting event handlers when the DOM is ready
 */
document.addEventListener('DOMContentLoaded', function() {
    registerButton.addEventListener('click', function (event) {
        event.preventDefault();
        registerUser();
    }, false);
}, false);

/*
 * Register via REST API, assuming that the backend runs at http://localhost:8000/,
 * which is Laravel dev server's default anyway
 */
function registerUser() {
    fetch(backendUrl, {
        body: new FormData(registerForm),
        method: 'POST',
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Accept': 'application/json'
        },
    }).then(response => response.json())
        .then(data => {
            console.log(data);
            setCookie('blhAccessTokenCookie', data.accessToken, 7);
            window.location.href = '/all-users';
    })
}
