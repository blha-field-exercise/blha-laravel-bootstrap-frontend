/**
 * Author: Siegfried Schweizer <sigi@sigi-schweizer.de> 2023
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * From now on only VanillaJS please :)
 *
 */

/*
 * Setting up global values and variables
 */
const loginForm = document.getElementById('login-form');
const loginButton = document.getElementById('login-button');
const backendUrl = 'http://127.0.0.1:8000/api/auth/login';

/*
 * Setting event handlers when the DOM is ready
 */
document.addEventListener('DOMContentLoaded', function() {
    loginButton.addEventListener('click', function (event) {
        event.preventDefault();
        loginUser();
    }, false);
}, false);

/*
 * Login via REST API, assuming that the backend runs at http://localhost:8000/,
 * which is Laravel dev server's default anyway
 */
function loginUser() {
    fetch(backendUrl, {
        body: new FormData(loginForm),
        method: 'POST',
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
            'Accept': 'application/json'
        },
    }).then(response => response.json())
        .then(data => {
            console.log(data);
            setCookie('blhAccessTokenCookie', data.accessToken, 7);
            window.location.href = '/all-users';
    })
}
