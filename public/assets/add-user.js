/**
 * Author: Siegfried Schweizer <sigi@sigi-schweizer.de> 2023
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * From now on only VanillaJS please :)
 *
 */

/*
 * Setting up global values and variables
 */
const addUserHeadline = document.getElementById('add-user-headline');
const addUserForm = document.getElementById('add-user-form');
const addUserButton = document.getElementById('add-user-button');
const accessToken = getCookie('blhAccessTokenCookie');
const backendUrl = 'http://127.0.0.1:8000/api/auth/add-user';

/*
 * Setting event handlers and actions when the DOM is ready
 */
document.addEventListener('DOMContentLoaded', function() {
    if (accessToken) {
        addUserButton.addEventListener('click', function (event) {
            event.preventDefault();
            addUser();
        }, false);
    } else {
        addUserHeadline.innerHTML = 'Unauthorized';
        addUserForm.classList.add('d-none');
    }
}, false);

/*
 * Adds a new user with our backend API
 */
function addUser() {
    fetch(backendUrl, {
        body: new FormData(addUserForm),
        method: 'POST',
        headers: {
            'Authorization': 'Bearer ' + accessToken,
            'X-Requested-With': 'XMLHttpRequest',
            'Accept': 'application/json'
        },
    }).then(response => response.json())
        .then(data => {
            console.log(data);
            window.location.href = '/all-users';
        })
}
