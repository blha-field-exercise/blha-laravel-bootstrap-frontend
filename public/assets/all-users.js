/**
 * Author: Siegfried Schweizer <sigi@sigi-schweizer.de> 2023
 *
 * The JavaScript code in this page is free software: you can
 * redistribute it and/or modify it under the terms of the GNU
 * General Public License (GNU GPL) as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option)
 * any later version.  The code is distributed WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU GPL for more details.
 *
 * From now on only VanillaJS please :)
 *
 */

/*
 * Setting up global values and variables
 */
const allUsersList = document.getElementById('all-users-list');
const accessToken = getCookie('blhAccessTokenCookie');
const backendUrl = 'http://127.0.0.1:8000/api/auth/users';

/*
 * Setting actions when the DOM is ready
 */
document.addEventListener('DOMContentLoaded', function() {
    let h1Element = document.createElement('h1');
    h1Element.classList.add('h3', 'mb-3', 'fw-normal');
    if (accessToken) {
        h1Element.innerHTML = 'All users';
        getAllUsers();
    } else {
        h1Element.innerHTML = 'Unauthorized';
    }
    allUsersList.appendChild(h1Element);
}, false);

/*
 * Gets all users from our backend API
 */
function getAllUsers() {
    fetch(backendUrl, {
        method: 'GET',
        headers: {
            'Authorization': 'Bearer ' + accessToken,
            'X-Requested-With': 'XMLHttpRequest',
            'Accept': 'application/json'
        },
    }).then(response => response.json())
        .then(data => {
            console.log(data);
            writeUsersToDOM(data);
        })
}

/*
 * Writes users data to the DOM
 */
function writeUsersToDOM(data) {
    Array.from(data).forEach(
        function(element, index, array) {
            console.log(element);

            let aElement = document.createElement('a');
            aElement.classList.add('list-group-item', 'list-group-item-action', 'd-flex', 'gap-3', 'py-3');
            aElement.setAttribute('aria-current', 'true');

            let imgElement = document.createElement('img');
            imgElement.src = 'https://github.com/twbs.png';
            imgElement.alt = 'twbs';
            imgElement.width = '32';
            imgElement.height = '32';
            imgElement.classList.add('rounded-circle', 'flex-shrink-0');
            aElement.appendChild(imgElement);

            let divElement = document.createElement('div');
            divElement.classList.add('d-flex', 'gap-2', 'w-100', 'justify-content-between');
            aElement.appendChild(divElement);

            let nestedDivElement = document.createElement('div');
            let h6Element = document.createElement('h6');
            h6Element.classList.add('mb-0');
            h6Element.innerHTML = element.name;
            nestedDivElement.appendChild(h6Element);

            let pElement = document.createElement('p');
            pElement.classList.add('mb-0', 'opacity-75');
            pElement.innerHTML = element.email;
            nestedDivElement.appendChild(pElement);

            divElement.appendChild(nestedDivElement);
            allUsersList.appendChild(aElement);
        }
    );
    let aButtonElement = document.createElement('a')
    aButtonElement.href = '/add-user';
    aButtonElement.classList.add('btn', 'btn-primary', 'w-100', 'py-2', 'mt-4');
    //aButtonElement.setAttribute('role', 'button');
    aButtonElement.innerHTML = 'Add a new user';
    allUsersList.appendChild(aButtonElement);

    let formElement = document.createElement('form');






}
